package ar.edu.unlu.poo.ejercicio11;

import java.util.ArrayList;
import java.util.Arrays;

public class Diccionario {

    private static ArrayList<String> diccionario = new ArrayList<String>(Arrays.asList("naturaleza", "campo",
            "bosque", "selva", "jungla", "desierto", "costa", "playa", "río", "laguna", "lago", "mar",
            "océano", "cerro", "monte", "montaña", "luz", "energía", "ambiente", "espacio", "entorno",
            "área", "superficie", "volumen", "región", "zona", "lado", "mundo", "planeta", "sol", "luna",
            "estrella", "galaxia", "universo", "clima", "queso", "walter"));

    public String getDiccionario() {
        String s = "";
        for(String palabra: diccionario) {
            s += "\n" + palabra;
        }
        return s;
    }
    public void agregarPalabra(String palabra) {
        palabra = palabra.toLowerCase();
        if(!diccionario.contains(palabra)) {
            diccionario.add(palabra);
        }
    }
    public boolean isPalabraValida(String palabra) {
        return diccionario.contains(palabra.toLowerCase());
    }

    public int calcularPuntaje(String palabra) {
        int puntos = 0;
        palabra = palabra.toLowerCase();
        if(isPalabraValida(palabra)) {
            puntos = calcularPuntos(palabra);
        }
        return puntos;
    }

    private int calcularPuntos(String palabra) {
        int puntos = 0;
        char letra;
        for(int i = 0; i < palabra.length(); i++) {
            letra = palabra.charAt(i);
            if(letra == 'k' || letra == 'z' || letra == 'x' || letra == 'y' || letra == 'w' || letra == 'q') {
                puntos += 2;
            }
            else {
                puntos += 1;
            }
        }
        return puntos;
    }
}
