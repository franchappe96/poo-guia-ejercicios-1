package ar.edu.unlu.poo.ejercicio11;

import java.util.ArrayList;

public class Jugador {

    private String nombre;
    private ArrayList<Integer> puntajes = new ArrayList<Integer>();
    private ArrayList<String> palabrasValidas = new ArrayList<String>();

    public Jugador(String nombre) {
        setNombre(nombre);
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public int getPuntajeTotal() {
        int puntos = 0;
        for(int puntaje: puntajes) {
            puntos += puntaje;
        }
        return puntos;
    }

    public String getPalabrasValidas() {
        String s = "";
        for(String palabra: palabrasValidas) {
            s += palabra + ";";
        }
        return s;
    }

    public int formarPalabra(String palabra) {
        Diccionario d = new Diccionario();
        int puntos = 0;
        // El mismo jugador no debería repetir una palabra
        if(!palabrasValidas.contains(palabra)) {
            puntos = d.calcularPuntaje(palabra);
            if(puntos > 0) {
                palabrasValidas.add(palabra);
                puntajes.add(puntos);
            }
        }
        return puntos;
    }
}
