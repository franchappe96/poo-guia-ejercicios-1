package ar.edu.unlu.poo.ejercicio11;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingrese la cantidad de rondas que desea jugar: ");
        int rondas = scanner.nextInt();
        scanner = new Scanner(System.in);
        System.out.println("Ingrese el nombre del Jugador 1: ");
        String nombreJ1 = scanner.nextLine();
        scanner = new Scanner(System.in);
        System.out.println("Ingrese el nombre del Jugador 2: ");
        String nombreJ2 = scanner.nextLine();
        Jugador j1 = new Jugador(nombreJ1);
        Jugador j2 = new Jugador(nombreJ2);
        String palabra;
        while(rondas != 0) {
            System.out.println("-------Turno de " + j1.getNombre() + "-------");
            scanner = new Scanner(System.in);
            System.out.println("Ingrese una palabra: ");
            palabra = scanner.nextLine();
            j1.formarPalabra(palabra);
            System.out.println("-------Turno de " + j2.getNombre() + "-------");
            scanner = new Scanner(System.in);
            System.out.println("Ingrese una palabra: ");
            palabra = scanner.nextLine();
            j2.formarPalabra(palabra);
            rondas--;
        }
        System.out.println("----------RECUENTO DE PUNTOS----------");
        System.out.println(j1.getNombre() + ": " + j1.getPalabrasValidas());
        System.out.println("Puntaje Total: " + j1.getPuntajeTotal());
        System.out.println("---------------------------------------");
        System.out.println(j2.getNombre() + ": " + j2.getPalabrasValidas());
        System.out.println("Puntaje Total: " + j2.getPuntajeTotal());
    }

}
