package ar.edu.unlu.poo.ejercicio2;

public class Nodo {
    private Nodo siguiente;
    private Nodo anterior;
    private Object dato;
    public Nodo(Object valor) {
        this.setDato(valor);
    }
    public void setDato(Object valor) {
        dato = valor;
    }
    public Object getDato() {
        return dato;
    }
    public void setSiguiente(Nodo siguiente) {
        this.siguiente = siguiente;
    }
    public Nodo getSiguiente() {
        return siguiente;
    }
    public void setAnterior(Nodo anterior) {
        this.anterior = anterior;
    }
    public Nodo getAnterior() {
        return anterior;
    }

}
