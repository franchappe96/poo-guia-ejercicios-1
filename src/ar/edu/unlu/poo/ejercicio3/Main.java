package ar.edu.unlu.poo.ejercicio3;
public class Main {
    public static void main(String[] args) {
        Pila p = new Pila();
        p.apilar(2);
        p.apilar(3);
        p.apilar(4);
        p.apilar(5);
        System.out.println(p.getTope());
        p.desapilar();
        p.apilar(6);
        System.out.println(p.toString());
        System.out.println(p.toString());
    }
}
