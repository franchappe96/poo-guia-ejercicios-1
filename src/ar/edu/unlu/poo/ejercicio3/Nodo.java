package ar.edu.unlu.poo.ejercicio3;

public class Nodo {
    private Object valor;
    private Nodo siguiente = null;
    public Nodo(Object dato) {
        setValor(dato);
    }
    public void setValor(Object dato) {
        valor = dato;
    }
    public void setSiguiente(Nodo siguiente) {
        this.siguiente = siguiente;
    }
    public Object getValor() {
        return valor;
    }
    public Nodo getSiguiente() {
        return siguiente;
    }

}
