package ar.edu.unlu.poo.ejercicio3;

public class Pila {
    private Nodo tope = null;
    public boolean isVacia() {
        return tope == null;
    }
    public Object getTope() {
        return tope.getValor();
    }
    public void apilar(Object valor) {
        Nodo nuevoTope = new Nodo(valor);
        if (isVacia()) {
            tope = nuevoTope;
        }
        else {
            nuevoTope.setSiguiente(tope);
            tope = nuevoTope;
        }
    }
    public Object desapilar() {
        Object resultado = null;
        if (!isVacia()) {
            resultado = tope.getValor();
            tope = tope.getSiguiente();
        }
        return resultado;
    }
    public String toString() {
        String s = "";
        Pila aux = new Pila();
        if(isVacia()) {
            s = "La pila se encuentra vacía";
        }
        else {
            while(tope != null) {
                aux.apilar(tope.getValor());
                s += "\n" + desapilar().toString();
            }
        }
        intercambiar(aux);
        return s;
    }
    private void intercambiar(Pila aux) {
        while (aux.tope != null) {
            apilar(aux.desapilar());
        }
    }
}
