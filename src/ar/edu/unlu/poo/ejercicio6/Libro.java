package ar.edu.unlu.poo.ejercicio6;

public class Libro {
    private String isbn;
    private String titulo;
    private String autor;
    private int paginas;
    private int ejemplaresTotal;
    static int ejemplaresPrestados = 0;
    private int ejemplaresDisponibles;

    public Libro(String titulo, String autor, int paginas) {
        this(titulo, autor, paginas, "", 0);
    }
    public Libro(String titulo, String autor, int paginas, String isbn, int ejemplares) {
        setTitulo(titulo);
        setAutor(autor);
        setIsbn(isbn);
        setPaginas(paginas);
        setEjemplaresTotal(ejemplares);
        setEjemplaresDisponibles();
    }
    private void setPaginas(int paginas) {
        this.paginas = paginas;
    }
    private void setIsbn(String isbn) {
        this.isbn = isbn;
    }
    private void setAutor(String autor) {
        this.autor = autor;
    }
    private void setTitulo(String titulo) {
        this.titulo = titulo;
    }
    private void setEjemplaresDisponibles() {
        ejemplaresDisponibles = ejemplaresTotal;
    }
    public void setEjemplaresTotal(int cantidad) {
        ejemplaresTotal = cantidad;
    }
    public boolean prestar() {
        boolean resultado = false;
        if (ejemplaresDisponibles > 1) {
            ejemplaresDisponibles--;
            ejemplaresPrestados++;
            resultado = true;
        }
        return resultado;
    }
    public boolean devolver() {
        boolean resultado = false;
        if (ejemplaresDisponibles + 1 <= ejemplaresTotal) {
            ejemplaresDisponibles++;
            resultado = true;
        }
        return resultado;
    }

    public String toString() {
        return "El libro " + getTitulo() + " creado por el autor " + getAutor() +
                " tiene " + getPaginas() + " páginas, quedan " + getDisponibles() + " disponibles y se prestaron "
                + (getEjemplaresTotal() - getDisponibles()) + " ejemplar/es";
    }

    public int getEjemplaresTotal() {
        return ejemplaresTotal;
    }

    public int getDisponibles() {
        return ejemplaresDisponibles;
    }

    public int getPaginas() {
        return paginas;
    }

    public String getAutor() {
        return autor;
    }

    public String getTitulo() {
        return titulo;
    }

}


