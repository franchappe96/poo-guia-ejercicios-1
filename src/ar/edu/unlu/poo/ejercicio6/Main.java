package ar.edu.unlu.poo.ejercicio6;

public class Main {

    public static void main(String[] args) {
        Libro libro = new Libro("Libro1", "Alguien", 91, "1028732", 1);
        Libro libro2 = new Libro("Libro2", "Fulano", 90, "1028735", 10);
        System.out.println(libro.toString());
        System.out.println(libro2.toString());
        if(libro.getPaginas() > libro2.getPaginas()) {
            System.out.println("El libro " + libro.getTitulo() + " tiene más páginas que el libro " + libro2.getTitulo());
        }
        else if(libro2.getPaginas() > libro.getPaginas()){
            System.out.println("El libro " + libro2.getTitulo() + " tiene más páginas que el libro " + libro.getTitulo());
        }
        else {
            System.out.println("Los libros " + libro.getTitulo() + " y " + libro2.getTitulo()
                    + " tienen la misma cantidad de páginas");
        }
        if(libro.prestar()) {
            System.out.println("El libro " + libro.getTitulo() + " fue prestado correctamente.");
        }
        else {
            System.out.println("El libro " + libro.getTitulo() + " NO puede ser prestado.");
        }
        if(libro2.prestar()) {
            System.out.println("El libro " + libro2.getTitulo() + " fue prestado correctamente.");
        }
        else {
            System.out.println("El libro " + libro2.getTitulo() + " NO puede ser prestado.");
        }
        System.out.println("El total de prestamos realizados es: " + libro.getEjemplaresTotal());
    }
}
