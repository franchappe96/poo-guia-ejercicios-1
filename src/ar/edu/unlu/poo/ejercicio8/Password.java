package ar.edu.unlu.poo.ejercicio8;

import java.util.Random;

public class Password {
    private int longitud;
    private String password;

    private boolean fuerte;

    private static String caracteresPosibles = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    public Password() {
        this(8);
    }
    public Password(int longitud) {
        setLongitud(longitud);
        password = generarClave(longitud);
    }
    public void setLongitud(int longitud) {
        this.longitud = longitud;
    }
    public int getLongitud() {
        return longitud;
    }

    public String generarClave(int longitud) {
        Random random = new Random();
        String clave = "";
        int minusculas = 0, mayusculas = 0, numeros = 0;
        int indice;
        for(int i = 0; i < longitud; i++) {
            indice = random.nextInt(caracteresPosibles.length());
            clave += caracteresPosibles.charAt(indice);
            if(indice <= 25) {
                mayusculas++;
            }
            else if(indice <= 51) {
                minusculas++;
            }
            else {
                numeros++;
            }
        }
        setFuerte(mayusculas > 2 && minusculas > 1 && numeros >= 2);
        return clave;
    }

    public String getPassword() {
        return password;
    }

    public boolean getFuerte() {
        return fuerte;
    }

    private void setFuerte(boolean valor) {
        fuerte = valor;
    }

    public void hacerFuerte() {
        while(!getFuerte()) {
            password = generarClave(longitud);
            System.out.println(password);
        }
    }
}
