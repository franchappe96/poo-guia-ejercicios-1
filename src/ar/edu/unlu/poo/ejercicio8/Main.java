package ar.edu.unlu.poo.ejercicio8;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ArrayList<Password> listaClaves = new ArrayList<Password>();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingrese la cantidad de contraseñas a generar: ");
        int cantidad = scanner.nextInt();
        System.out.println("Ingrese la cantidad de caracteres de las claves: ");
        int caracteres = scanner.nextInt();
        for(int i = 0; i < cantidad; i++) {
            listaClaves.add(new Password(caracteres));
        }
        String s = "";
        for(Password clave: listaClaves) {
            if(clave.getFuerte()) {
                s = "Fuerte";
            }
            else {
                s = "Débil";
            }
            System.out.println(clave.getPassword() + " - " + s);
        }
        Password clave = new Password(8);
        System.out.println(clave.getPassword());
        System.out.println(clave.getFuerte());
        clave.hacerFuerte();
        System.out.println(clave.getPassword());
        System.out.println(clave.getFuerte());
    }
}
