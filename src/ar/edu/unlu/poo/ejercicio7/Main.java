package ar.edu.unlu.poo.ejercicio7;

import static java.lang.Math.sqrt;

public class Main {
    public static void main(String[] args) {
        Ecuacion e = new Ecuacion(1, 4, 5);
        Ecuacion e2 = new Ecuacion(1, -5, 6);
        Ecuacion e3 = new Ecuacion(1, -6, 9);
        System.out.println(e.getRaices().toString());
        System.out.println(e.tieneRaiz());
        System.out.println(e2.getRaices().toString());
        System.out.println(e2.tieneRaiz());
        System.out.println(e3.getRaices().toString());
        System.out.println(e3.tieneRaiz());
    }
}
