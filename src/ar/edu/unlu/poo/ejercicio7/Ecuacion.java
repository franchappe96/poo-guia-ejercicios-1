package ar.edu.unlu.poo.ejercicio7;

import java.awt.image.AreaAveragingScaleFilter;
import java.util.ArrayList;

import static java.lang.Math.sqrt;

public class Ecuacion {
    private final int a;
    private final int b;
    private final int c;
    private ArrayList<Double> raices = new ArrayList<>();

    public Ecuacion(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
        establecerRaices();
    }
    private void establecerRaices() {
        double r1 = (-b + sqrt((b*b) - (4 * a * c))) / (2 * a);
        double r2 = (-b - sqrt((b*b) - (4 * a * c))) / (2 * a);
        if(!Double.isNaN(r1)) {
            if((b*b) - 4 * a * c == 0) {
                raices.add(r1);
            }
            else {
                raices.add(r1);
                raices.add(r2);
            }
        }
    }
    public ArrayList<Double> getRaices() {
        return raices;
    }
    public boolean tieneRaiz() {
        return !raices.isEmpty();
    }
    public int calcularY(int x) {
        int resultado = a * (x*x) + b * x + c;
        return resultado;
    }
}
