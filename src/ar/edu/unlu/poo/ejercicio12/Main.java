package ar.edu.unlu.poo.ejercicio12;

import ar.edu.unlu.poo.ejercicio2.Lista;
import ar.edu.unlu.poo.ejercicio5.Estado;
import ar.edu.unlu.poo.ejercicio5.Prioridad;
import ar.edu.unlu.poo.ejercicio5.Tarea;

public class Main {
    public static void main(String[] args) {
        Administrador a = new Administrador();
        Tarea t = new Tarea("Lavar el auto", Estado.INCOMPLETA, Prioridad.ALTA, "2023-07-06");
        Tarea t2 = new Tarea("Ir al cine", Estado.INCOMPLETA, Prioridad.ALTA, "2023-07-05");
        Tarea t3 = new Tarea("Ir al supermercado", Estado.INCOMPLETA, Prioridad.ALTA, "2023-07-02");
        Tarea t4 = new Tarea("Estudiar para el examen", Estado.INCOMPLETA, Prioridad.ALTA, "2023-07-03");
        a.agregarTarea(t);
        a.agregarTarea(t2);
        a.agregarTarea(t3);
        a.agregarTarea(t4);
        System.out.println("-----Listado de Tareas-----");
        System.out.println(a.getListaTareas());
        System.out.println("-----Listado de Tareas Ordenadas por Prioridad - Fecha de Vencimiento-----");
        System.out.println(a.pendientesOrdenadas().mostrar());
        System.out.println("----------------------------------------------");
        a.completarTarea("Ir al cine", "Franco Chappe", "2023-09-19");
        a.completarTarea("Lavar el auto", "Colaborador 2", "2023-09-19");
        a.completarTarea("Ir al supermercado", "Franco Chappe", "2023-09-19");
        System.out.println("-----Lista de tareas completadas por Franco Chappe-----");
        Lista l = a.tareasPorColaborador("Franco Chappe");
        System.out.println(l.mostrar());
    }

}
