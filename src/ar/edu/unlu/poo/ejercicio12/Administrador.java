package ar.edu.unlu.poo.ejercicio12;

import ar.edu.unlu.poo.ejercicio2.Lista;
import ar.edu.unlu.poo.ejercicio5.Estado;
import ar.edu.unlu.poo.ejercicio5.Tarea;

import java.util.ArrayList;

public class Administrador {

    private Lista listaTareas = new Lista();
    private ArrayList<String> colaboradores = new ArrayList<String>();

    public void agregarTarea(Tarea t) {
        listaTareas.agregar(t);
    }

    public String getListaTareas() {
        String s = "";
        Tarea t;
        for(int i = 1; i <= listaTareas.getLongitud(); i++) {
            t = (Tarea)listaTareas.recuperar(i);
            s += t.getDescripcion() + "\n";
        }
        return s;
    }
    public Tarea buscarTarea(String titulo) {
        boolean encontrada = false;
        int i = 1;
        Tarea t, tRes = null;
        while(!encontrada && i <= listaTareas.getLongitud()) {
            t = (Tarea)listaTareas.recuperar(i);
            if (t.getDescripcion() == titulo) {
                tRes = t;
                encontrada = true;
            }
            i++;
        }
        return tRes;
    }

    public Lista tareasPorColaborador(String colaborador) {
        Lista tareas = new Lista();
        Tarea t;
        for(int i = 1; i <= listaTareas.getLongitud(); i++) {
            t = (Tarea)listaTareas.recuperar(i);
            if(t.getColaborador() == colaborador) {
                tareas.agregar(t);
            }
        }
        return tareas;
    }

    public void agregarColaborador(String colaborador) {
        if(!colaboradores.contains(colaborador)) {
            colaboradores.add(colaborador);
        }
    }

    public void completarTarea(String titulo, String colaborador, String fecha) {
        Tarea t = buscarTarea(titulo);
        if(t != null) {
            agregarColaborador(colaborador);
            t.setEstado(Estado.COMPLETA);
            t.setColaborador(colaborador);
            t.setFechaFinalizacion(fecha);
            System.out.println("La tarea fue marcada como COMPLETA");
        }
        else {
            System.out.println("La tarea que desea modificar no existe");
        }
    }

    public Lista pendientesOrdenadas() {
        Lista lRes = new Lista();
        Tarea t;
        for(int i = 1; i <= listaTareas.getLongitud(); i++) {
            t = (Tarea)listaTareas.recuperar(i);
            if (!t.isVencida()) {
                insertarOrdenado(lRes, t);
            }
        }
        return lRes;
    }

    public void insertarOrdenado(Lista l, Tarea t) {
        if(l.getLongitud() == 0) {
            l.agregar(t);
        }
        else {
            Tarea tAux;
            int i = 1;
            boolean insertado = false;
            while(i <= l.getLongitud() && !insertado) {
                tAux = (Tarea)l.recuperar(i);
                if(t.getPrioridad().ordinal() <= tAux.getPrioridad().ordinal() && t.getFechaLimite().isBefore(tAux.getFechaLimite())) {
                    l.insertar(t, i);
                    insertado = true;
                }
                i++;
            }
            if(!insertado) {
                l.agregar(t);
            }
        }
    }

}
