package ar.edu.unlu.poo.ejercicio4;

public class Nodo {
    Nodo siguiente = null;
    Object valor;
    public Nodo(Object dato) {
        setValor(dato);
    }
    private void setValor(Object dato) {
        valor = dato;
    }
    public void setSiguiente(Nodo siguiente) {
        this.siguiente = siguiente;
    }
    public Object getValor() {
        return valor;
    }

    public Nodo getSiguiente() {
        return siguiente;
    }

}
