package ar.edu.unlu.poo.ejercicio4;

public class Cola {
    private Nodo primero = null;

    public boolean isVacia() {
        return primero == null;
    }
    public Object getPrimero() {
        return primero.getValor();
    }
    public void encolar(Object valor) {
        Nodo ultimo = new Nodo(valor);
        if(isVacia()) {
            primero = ultimo;
        }
        else {
            Nodo actual = primero;
            while(actual.getSiguiente() != null) {
                actual = actual.getSiguiente();
            }
            actual.setSiguiente(ultimo);
        }
    }

    public Object desencolar() {
        Object dato = null;
        if(!isVacia()) {
            dato = primero.getValor();
            primero = primero.getSiguiente();
        }
        return dato;
    }
    public String toString() {
        String s = "";
        if(isVacia()) {
            s = "La cola se encuentra vacía";
        }
        else {
            Nodo actual = primero;
            while(actual.getSiguiente() != null) {
                s += "\t" + actual.getValor().toString();
                actual = actual.getSiguiente();
            }
            s += "\t" + actual.getValor().toString();
        }
        return s;
    }
}
