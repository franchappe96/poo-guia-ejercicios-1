package ar.edu.unlu.poo.ejercicio4;

public class Main {
    public static void main(String[] args) {
        Cola c = new Cola();
        c.encolar(5);
        c.encolar(8);
        System.out.println(c.desencolar().toString());
        c.desencolar();
        c.desencolar();
        c.desencolar();
        System.out.println(c.toString());
        c.encolar(2);
        System.out.println(c.toString());
    }
}
