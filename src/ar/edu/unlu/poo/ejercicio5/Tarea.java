package ar.edu.unlu.poo.ejercicio5;

import java.time.LocalDate;
import java.util.ArrayList;

public class Tarea {
    private Estado estado;
    private Prioridad prioridad;
    private String descripcion;
    private LocalDate fechaLimite;
    private LocalDate fechaRecordatorio;
    private LocalDate fechaFinalizacion;
    private String colaborador;

    public Tarea(String descripcion, Estado estado, Prioridad prioridad) {
        this(descripcion, estado, prioridad, null);
    }

    public Tarea(String descripcion, Estado estado, Prioridad prioridad, String fecha) {
        setDescripcion(descripcion);
        setEstado(estado);
        setPrioridad(prioridad);
        setFecha(fecha);
    }
    public void setFecha(String fecha) {
        try {
            fechaLimite = LocalDate.parse(fecha);
        }
        catch (Exception e) {
            fechaLimite = null;
        }
    }

    public void setFechaFinalizacion(String fecha) {
        try {
            fechaFinalizacion = LocalDate.parse(fecha);
        }
        catch (Exception e) {
            fechaFinalizacion = null;
        }
    }

    public void setPrioridad(Prioridad prioridad) {
        this.prioridad = prioridad;
    }
    public void setEstado(Estado estado) {
        this.estado = estado;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public void setColaborador(String c) {
        colaborador = c;
    }
    public String toString() {
        String s = getDescripcion();
        if (getEstado() == Estado.COMPLETA) {
            s = "(Vencida) " + s;
        }else if(fechaRecordatorio != null && (fechaRecordatorio.isAfter(fechaLimite)|| (fechaRecordatorio.isEqual(fechaLimite)))) {
            s = "(Por Vencer) " + s;
            setPrioridad(Prioridad.ALTA);
        }
        return s;
    }

    public Estado getEstado() {
        return estado;
    }
    public String getDescripcion() {
        return descripcion;
    }

    public String getColaborador() {
        return this.colaborador;
    }

    public Prioridad getPrioridad() {
        return prioridad;
    }

    public String getFecha() {
        String fecha = "La tarea no tiene una fecha límite";
        if (fechaLimite != null) {
            fecha = fechaLimite.toString();
        }
        return fecha;
    }
    public boolean isVencida() {
        return estado == Estado.VENCIDA;
    }
    public boolean isCompleta() {
        return estado == Estado.COMPLETA;
    }

    public void setFechaRecordatorio(String fecha) {
        try {
            fechaRecordatorio = LocalDate.parse(fecha);
        }
        catch (Exception e) {
            fechaRecordatorio = null;
        }
    }
    public LocalDate getFechaLimite() {
        return fechaLimite;
    }

}
