package ar.edu.unlu.poo.ejercicio5;

import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {
        Tarea supermercado = new Tarea("Ir al supermercado mañana", Estado.INCOMPLETA,
                Prioridad.MEDIA, "2023-09-06");
        LocalDate ayer = LocalDate.now().plusDays(-1);
        Tarea repuesto = new Tarea("Consultar repuesto del auto", Estado.COMPLETA,
                Prioridad.ALTA, ayer.toString());
        Tarea cine = new Tarea("Ir al cine a ver la nueva película de Marvel", Estado.INCOMPLETA,
                Prioridad.BAJA, ayer.toString());
        supermercado.setFechaRecordatorio("2023-09-07");
        System.out.println(supermercado.toString());
        System.out.println(supermercado.getPrioridad());
    }
}
