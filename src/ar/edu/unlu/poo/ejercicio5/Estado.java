package ar.edu.unlu.poo.ejercicio5;

public enum Estado {
    COMPLETA,
    INCOMPLETA,
    VENCIDA
}
