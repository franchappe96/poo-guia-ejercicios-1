package ar.edu.unlu.poo.ejercicio1;

public class Main {

    public static void main(String[] args) {
        Lista l1 = new Lista(5);
        l1.cargaAleatoriaInt();
        System.out.println(l1.mostrar());
        l1.eliminar(1);
        l1.eliminar(1);
        l1.eliminar(1);
        l1.eliminar(1);
        System.out.println(l1.mostrar());
    }
}
