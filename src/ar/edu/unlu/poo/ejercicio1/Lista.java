package ar.edu.unlu.poo.ejercicio1;

public class Lista {
    private Nodo inicio;
    private int longitud;
    public Lista(int cantidad) {
        if (cantidad < 0) {
            cantidad = 0;
        }
        longitud = cantidad;
        if (cantidad > 0) {
            inicio = new Nodo(null);
            Nodo actual = getInicio();
            for (int i = 1; i < cantidad; i++) {
                actual.setSiguiente(new Nodo(null));
                actual = actual.getSiguiente();
            }
        }
    }
    public Lista() {
        this(0);
    }
    public boolean isVacia() {
        return longitud == 0;
    }

    public void agregar(Object dato) {
        if(isVacia()) {
            inicio = new Nodo(dato);
        }
        else {
            Nodo actual = getInicio();
            while (actual.getSiguiente() != null) {
                actual = actual.getSiguiente();
            }
            actual.setSiguiente(new Nodo(dato));
        }
        longitud++;
    }

    public boolean eliminar(int posicion) {
        boolean eliminado = false;
        if (posicionValida(posicion)) {
            Nodo anterior = getInicio();
            if (posicion == 1) {
                inicio = anterior.getSiguiente();
            }
            else {
                // El bucle frena uno antes del Nodo que se desea eliminar
                for(int i = 1; i < posicion - 1; i++) {
                    anterior = anterior.getSiguiente();
                }
                Nodo eliminar = anterior.getSiguiente();
                // El anterior al que se desea eliminar debe apuntar al siguiente del Nodo Eliminar
                anterior.setSiguiente(eliminar.getSiguiente());
            }
            longitud--;
            eliminado = true;
        }
        return eliminado;
    }

    public boolean insertar(Object dato, int posicion) {
        boolean insertado = false;
        if(posicionValida(posicion)) {
            Nodo nuevo = new Nodo(dato);
            if(posicion == 1) {
                nuevo.setSiguiente(getInicio());
                setInicio(nuevo);
            }
            else {
                Nodo anterior = getInicio();
                for(int i = 1; i < posicion - 1; i++) {
                    anterior = anterior.getSiguiente();
                }
                // El Nodo a insertar apunta al siguiente de su anterior
                nuevo.setSiguiente(anterior.getSiguiente());
                // El anterior apunta al nodo insertado
                anterior.setSiguiente(nuevo);
            }
            longitud++;
            insertado = true;
        }else if(posicion >= getLongitud()) {
            agregar(dato);
        }
        return insertado;
    }

    public Object recuperar(int posicion) {
        Object valor = null;
        if (posicionValida(posicion)) {
            Nodo actual = getInicio();
            for(int i = 1; i < posicion; i++) {
                actual = actual.getSiguiente();
            }
            valor = actual.getDato();
        }
        return valor;
    }

    public String mostrar() {
        String s = "";
        if(!isVacia()) {
            Nodo actual = getInicio();
            s = actual.getDato().toString();
            for(int i = 1; i < getLongitud(); i++) {
                actual = actual.getSiguiente();
                s = s + "\n" + actual.getDato().toString();
            }
        }
        return s;
    }

    public void cargaAleatoriaInt() {
        Nodo actual = getInicio();
        for(int i = 0; i < getLongitud(); i++) {
            actual.setDato((int)(Math.random() * 100 + 1));
            actual = actual.getSiguiente();
        }
    }

    private boolean posicionValida(int posicion) {
        return posicion > 0 && posicion <= longitud;
    }

    private void setInicio(Nodo inicio) {
        this.inicio = inicio;
    }

    private Nodo getInicio() {
        return inicio;
    }

    public int getLongitud() {
        return longitud;
    }

}
