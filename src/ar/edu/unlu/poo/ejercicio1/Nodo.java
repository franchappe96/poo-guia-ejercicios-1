package ar.edu.unlu.poo.ejercicio1;

public class Nodo {
    private Nodo siguiente;
    private Object dato;

    public Nodo(Object valor) {
        this.setDato(valor);
    }
    public void setDato(Object valor) {
        dato = valor;
    }
    public void setSiguiente(Nodo siguiente) {
        this.siguiente = siguiente;
    }
    public Nodo getSiguiente() {
        return siguiente;
    }
    public Object getDato() {
        return dato;
    }
}
