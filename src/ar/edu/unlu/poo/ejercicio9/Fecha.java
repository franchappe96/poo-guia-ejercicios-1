package ar.edu.unlu.poo.ejercicio9;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Fecha {

    private String[] formatos = {"dd-MM-yyyy", "MM-dd-yyyy"};

    public LocalDate getFecha(String s, String formato) {
        LocalDate fecha = null;
        if(isFormatoValido(formato)) {
            try{
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern(formato);
                fecha = LocalDate.parse(s, formatter);
            }
            catch (Exception e) {
            }
        };
        return fecha;
    }
    private boolean isFormatoValido(String formato) {
        boolean valido = false;
        for(String format: formatos) {
            if(format.equals(formato)) {
                valido = true;
            }
        }
        return valido;
    }

    public boolean esMayor(String fecha1, String fecha2, String formato) {
        boolean mayor = false;
        LocalDate f1 = getFecha(fecha1, formato);
        LocalDate f2 = getFecha(fecha2, formato);
        if(f1 != null && f2 != null) {
            mayor = f1.isAfter(f2);
        }
        return mayor;
    }

    public boolean esMenor(String fecha1, String fecha2, String formato) {
        boolean menor = false;
        LocalDate f1 = getFecha(fecha1, formato);
        LocalDate f2 = getFecha(fecha2, formato);
        if(f1 != null && f2 != null) {
            menor = f1.isBefore(f2);
        }
        return menor;
    }

    public boolean entreFecha(String fecha1, String fecha2, String fecha3, String formato) {
        return this.isFormatoValido(formato) && esMayor(fecha2, fecha1, formato) && esMenor(fecha2, fecha3, formato);
    }

}
