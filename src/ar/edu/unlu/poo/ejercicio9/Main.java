package ar.edu.unlu.poo.ejercicio9;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Main {
    public static void main(String[] args) {
        String fecha = "06-12-2023";
        String fecha2 = "07-12-2023";
        String fecha3 = "08-12-2023";
        String formato = "dd-MM-yyyy";
        Fecha f = new Fecha();
        System.out.println(f.getFecha(fecha, formato));
        System.out.println(f.esMayor(fecha, fecha2, formato));
        System.out.println(f.esMenor(fecha, fecha2, formato));
        System.out.println(f.entreFecha(fecha, fecha2, fecha3, formato));
    }
}
